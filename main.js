(function(silk){

	silk('.slider').slider({
		items: 3,
		responsive: true,
		desktopSlides: 2
	});


})(silk);
var info = {
	name: "Silk",
	description: "Small Component Library for the Silk Framework",
	author: "idfive"
}

function silk(el) {

	if (window === this) {
		return new silk(el);
	}

	this.e = document.querySelectorAll(el);
	return this;
}

/**
 * SilkSlider
 * A Light-weight responsive slider
 * @param obj
 */

silk.prototype.slider = function (obj) {
	// Slides
	var silkSlides = this.e;
	// Look through all sliders
	for (var i = 0; i < silkSlides.length; i++) {
		// Settings
		var settings = {
			items: obj.items || 2,
			margin: obj.margin || 20,
			speed: obj.speed || 1000,
			responsive: obj.responsive || true
		};

		// WindowWidth
		(function render() {
			var index = 0;
			// Slider
			var slider = silkSlides[i];
			// Children
			var children = slider.children;
			// Number of Slides
			var numberOfSlides = parseInt(children.length);
			// Container Width
			var containerWidth = slider.parentNode.offsetWidth;
			// Slide Item Width
			var slideItemWidth = (containerWidth / settings.items) - (settings.margin)
			// Slide Container Width
			var silkSliderContainerWidth = slideItemWidth * (numberOfSlides);
			// Adjust SilkSliderWidth
			var silkSliderWidth = ((slideItemWidth - (settings.margin * settings.items)) * settings.items) - (settings.items * settings.margin);

			var children = slider.children;
			// Rebuild
			for (child in children) {
				if (children[child].tagName) {
					// Replace LI to div
					var slideNode = document.createElement('div');
					slideNode.innerHTML = children[child].innerHTML;
					// slideNode.style.width = renderSlideOutput() + '%';
					slider.replaceChild(slideNode, children[child]);

					children[child].style.width = slideItemWidth - ((settings.margin * numberOfSlides)) + "px";

					if (parseInt(child) !== 0) {
						children[child].style.marginLeft = (settings.margin) + "px";
					}

					// Add Classes
					children[child].classList.add('silk-slider__item');
					children[child].classList.add('silk-slider__slide');
				}
			}

			// Silk Slider Wrapper updates 'ol' to 'div'
			var silkSlider = document.createElement('div');
			var silkContainer = document.createElement('div');

			// Class & Style
			silkSlider.classList.add('silk-slider');
			silkSlider.style.width = silkSliderWidth + 'px';
			// Silk Container
			silkContainer.classList.add('silk-slider__container');
			silkContainer.style.width = silkSliderContainerWidth + 'px';
			silkContainer.style.marginLeft = '0px';
			// Output
			silkContainer.innerHTML = slider.innerHTML;
			silkSlider.appendChild(silkContainer);
			slider.parentNode.replaceChild(silkSlider, slider);

			var controls = document.createElement('div');
			var prev = document.createElement('div');
			var next = document.createElement('div');
			controls.classList.add('silk-slider__controls');

			prev.classList.add('prev');
			prev.innerText = 'Prev';

			next.classList.add('next');
			next.innerText = 'Next';

			controls.appendChild(prev);
			controls.appendChild(next);
			silkSlider.appendChild(controls);
			var location = 0;
			prev.addEventListener('click', function () {

				var pages = numberOfSlides % index;

				if (index > 0) {
					location += ((slideItemWidth) - (settings.margin * numberOfSlides));
					silkContainer.style.marginLeft = location + "px";
					index -= 1;
				}

			});

			next.addEventListener('click', function () {
				var pages = numberOfSlides % settings.items;
				location += -((slideItemWidth) - (settings.margin * numberOfSlides));
				if (index > 0) {
					location += -settings.margin;
				}
				silkContainer.style.marginLeft = location + "px";
				index += 1;
			});

		})();

	}

}
